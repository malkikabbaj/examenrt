package com.example.examenrt;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.support.JdbcTransactionManager;

import javax.sql.DataSource;

@Configuration
public class JobConfiguration {
    @Bean
    public Job job(JobRepository jobRepository, Step step1, Step step2, Step partitionStep){
        return new JobBuilder("databaseExtraction", jobRepository)
                .start(step1)
                .next(step2)
                .next(partitionStep)
                .build();
    }

    @Bean
    public Step step1(JobRepository jobRepository, JdbcTransactionManager transactionManager){
        return new StepBuilder("databasePreparation", jobRepository)
                .tasklet(new FilePreparationTasklet(),transactionManager)
                .build();
    }

    @Bean
    public FlatFileItemReader<SalesCreditCard> creditCardDataReader(){
        return new FlatFileItemReaderBuilder<SalesCreditCard>()
                .name("CreditCardDataReader")
                .resource(new FileSystemResource("sales_credit_card_data.csv"))
                .delimited()
                .names("creditCardId","cardType","cardNumber","expMonth","expYear","modifiedDate")
                .linesToSkip(1)
                .fieldSetMapper(new BeanWrapperFieldSetMapper<>() {{
                    setTargetType(SalesCreditCard.class);
                }})
                .build();
    }

    @Bean
    public JdbcBatchItemWriter<SalesCreditCard> creditCardFileWriter(DataSource dataSource){
        String sql = "INSERT INTO CREDIT_CARD values (:creditCardId, :cardType, :cardNumber, :expMonth, :expYear, :modifiedDate)"
        return new JdbcBatchItemWriterBuilder<SalesCreditCard>()
                .dataSource(dataSource)
                .sql(sql)
                .beanMapped()
                .build();
    }

    @Bean
    public Step step2(JobRepository jobRepository,
                      JdbcTransactionManager transactionManager,
                      ItemReader<SalesCreditCard> creditCardDataReader,
                      ItemWriter<SalesCreditCard> creditCardFileWriter){
        return new StepBuilder("UploadingData",jobRepository)
                .<SalesCreditCard, SalesCreditCard>chunk(100,transactionManager)
                .reader(creditCardDataReader)
                .writer(creditCardFileWriter)
                .faultTolerant()
                .skipLimit(7)
                .retryLimit(3)
                .build();
    }

    @Bean
    public Partitioner FilePartitioner() {
        FileSystemResource fileSystemResource = new FileSystemResource("fichier.csv");
        int linesToSkip = 1;
        int linesPerRecord = 100;
        return new FilePartitioner(fileSystemResource, linesToSkip, linesPerRecord);
    }

    @Bean
    public Step partitionStep(JobRepository jobRepository,
                              Step processingStep,
                              Partitioner customFilePartitioner) {
        return new StepBuilder("partitionStep", jobRepository)
                .partitioner(processingStep.getName(), customFilePartitioner)
                .step(processingStep)
                .gridSize(20)
                .build();
    }
}

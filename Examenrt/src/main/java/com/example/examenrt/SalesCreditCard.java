package com.example.examenrt;

import java.util.Date;

public record SalesCreditCard(
        int creditCardId,
        String cardType,
        String cardNumber,
        int expMonth,
        int expYear,
        Date modifiedDate
) {
}
